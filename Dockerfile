# pull official base image
FROM python:3.8.4

# set work directory
WORKDIR /usr/src/app

# install dependencies
COPY ./requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt


# copy project
COPY . .

# expose port
EXPOSE 8000

# command
CMD ["python", "./manage.py", "runserver", "0.0.0.0:8000"]

